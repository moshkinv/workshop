﻿using System;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace workshop2.Repositories
{
    public class JsonFileRepository<T> : IRepository<T>
    {
        public string FilePath { get; set; }

        public JsonFileRepository(string filePath)
        {
            FilePath = filePath;
        }

        public async Task Create(T item)
        {
            var serializedData = JsonConvert.SerializeObject(item);
            await File.WriteAllTextAsync(FilePath, serializedData);
        }

        public async Task<T> Read()
        {
            var dataFromFile = await File.ReadAllTextAsync(FilePath);
            var data = JsonConvert.DeserializeObject<T>(dataFromFile);

            return data;
        }
    }
}
