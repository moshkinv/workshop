﻿using System;
using System.Threading.Tasks;

namespace workshop2.Repositories
{
    public interface IRepository<T>
    {
        Task Create(T item);
        Task<T> Read();
    }
}
