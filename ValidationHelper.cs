﻿using System;
using System.Text.RegularExpressions;

namespace workshop2
{
    public class ValidationHelper
    {
        public static bool IsNameValid(string name)
        {
            return name.Length >= 2;
        }

        public static bool IsPassportIsValid(string passportId)
        {
            return new Regex(@"^[A-z]{2}[0-9]{6}$").IsMatch(passportId);
        }

        public static bool IsDateOfBirthValid(DateTime dob)
        {
            return DateTime.Now.Year - dob.Year >= 14;
        }
    }
}
