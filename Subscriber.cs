﻿using System;
using workshop2.Tariffs;
using Newtonsoft.Json;

namespace workshop2
{
    public class Subscriber
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PassportId { get; set; }
        public DateTime DateOfBirth { get; set; }
        public decimal Balance { get; set; }
        public bool IsNeedInternet { get; set; }
        [JsonIgnore]
        public ITariff Tariff { get; set; }

        public Subscriber()
        {
            Balance = 50;
        }

        public Subscriber(string firstName, string lastName, string passportId, DateTime dob, bool isInternetNeed)
        {
            FirstName = firstName;
            LastName = lastName;
            PassportId = passportId;
            DateOfBirth = dob;
            Balance = 50;
            IsNeedInternet = isInternetNeed;
            Tariff = isInternetNeed ? (ITariff)new ZipperTariff() : (ITariff)new CheetahTariff();
        }

        public override string ToString()
        {
            return $"Subscriber {FirstName} {LastName}, passport {PassportId}, DOB: {DateOfBirth}" +
                    $"{Environment.NewLine} Tariff: {Tariff}";
        }
    }
}
